const util = require('./index.js')
const should = require('should')

describe('util 테스트', function() {
  it('uppercase 함수 테스트', function() {
    const r = util.upperCase('abc')
    r.should.be.equal('ABC')
  })
})
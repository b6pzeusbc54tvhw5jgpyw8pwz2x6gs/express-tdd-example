const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const userRouter = require('./controller/user.router')

const app = express();
const PORT = 3000

/*
express:
- 애플리케이션
    - [x] listen
- 미들웨어
    - [x] 로깅미들웨어
    - [x] 써드파티 미들웨어
    - [x] 에러 미들웨어
    - [x] 404, 500 에러 미들웨어 

- TDD
    - [x] user list
    - [x] user list limit success / error
    - [x] get one user success / error
    - [x] create user success / error

- 라우팅
    - [x] get, post 메소드
    - [x] Router 클래스
    
- 요청객체/응답객체
    - [x] send, status
*/

app.use(bodyParser.json())

if( process.env.NODE_ENV !== 'test' ) {
  app.use(morgan('dev'))
}

app.get('/', function(req, res) {
  res.send('hello world')
})

app.use('/users', userRouter )

app.use( function(req, res, next) {
  console.log('routes can not handle')
  const err = new Error('Not Found')
  err.status = 404;
  next(err);
})

app.use( function( err, req, res, next) {
  res.status( err.status || 500 )
  console.error( err )
  res.send('error');
})

module.exports = app
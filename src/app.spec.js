const app = require('./app')
const should = require('should')
const supertest = require('supertest');
const testApp = supertest(app)

describe('index 파일 테스트', function() {  

  let body

  before( function(done) {
    testApp.get('/users?limit=1')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) throw err
      body = res.body
      done()
    })
  })
  
  it('/users api test', function() {
    body.should.be.instanceOf(Array)
  })

  it('check user model', function() {
    body[0].name.should.be.String()
  })

  it('limit1 test', function() {
    body.length.should.be.equal(1)
  })
})

describe('users detail 테스트', function() {  
  
  const name = 'yoo'
  const age = 23

  it('username test', function(done) {
    const NAME = 'moon'
    testApp.get('/users?username='+NAME)
    .end(function(err, res) {
      if (err) throw err
      
      res.body.should.not.be.instanceOf(Array)
      res.body.should.be.instanceOf(Object)
      res.body.name.should.be.equal( NAME )
      done()
    })
  })

  it('test create user', function(done) {
    
    testApp.post('/users')
    .send({ name, age })
    .end(function(err, res) {
      if (err) throw err
      
      res.body.should.not.be.instanceOf(Array)
      res.body.should.be.instanceOf(Object)
      res.body.name.should.be.equal( name )
      res.body.age.should.be.equal( age )
      done()
    })
  })

  it('get created user', function(done) {
    testApp.get('/users?username='+name)
    .end(function(err, res) {
      if (err) throw err
      
      res.body.should.not.be.instanceOf(Array)
      res.body.should.be.instanceOf(Object)
      res.body.name.should.be.equal( name )
      res.body.age.should.be.equal( age )
      done()
    })
  })
})
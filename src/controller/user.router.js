const express = require('express')
const _ = require('underscore')
const bodyParser = require('body-parser')
const router = express.Router()

const userList = [
  { name: 'allice', age: 44 },
  { name: 'moon', age: 22 },
  { name: 'baby', age: 3 },
]

router.use(bodyParser.json())

router.get('/', function(req, res) {
  // throw new Error('db error')

  const limit = req.query.limit || 10
  if( req.query.username ) {
    const user = _.findWhere( userList, { name: req.query.username })
    res.send( user )
  } else {
    res.send(userList.slice(0,limit))
  }
})
  
router.post('/', function(req, res) {
  // throw new Error('db error')
    
  const body = req.body
  console.log( body )
  const newUser = { id: Date.now(), name: body.name, age: body.age }
  userList.push( newUser )
  res.send( newUser )
})

module.exports = router